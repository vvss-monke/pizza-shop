package pizzashop.repository;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;

public class PaymentRepository {
    protected static final Logger log = LogManager.getLogger(PaymentRepository.class);
    private static final String FILENAME = "data/payments.txt";
    private final List<Payment> paymentList;

    public PaymentRepository() {
        this.paymentList = new ArrayList<>();
        readPayments();
    }

    private void readPayments() {
        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(FILENAME)).getFile());
        try (var br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                Payment payment = getPayment(line);
                paymentList.add(payment);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Payment getPayment(String line) {
        Payment item;
        if (line == null || line.equals("")) return null;
        StringTokenizer st = new StringTokenizer(line, ",");
        int tableNumber = Integer.parseInt(st.nextToken());
        String type = st.nextToken();
        double amount = Double.parseDouble(st.nextToken());
        item = new Payment(tableNumber, PaymentType.valueOf(type), amount);
        return item;
    }

    public void add(Payment payment) {
        paymentList.add(payment);
        writeAll();
    }

    public List<Payment> getAll() {
        return paymentList;
    }

    public Payment get(Integer index){
        return paymentList.get(index);
    }

    public void writeAll() {
        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(FILENAME)).getFile());

        try (var bw = new BufferedWriter(new FileWriter(file))) {
            for (Payment p : paymentList) {
                log.info(p.toString());
                bw.write(p.toString());
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}