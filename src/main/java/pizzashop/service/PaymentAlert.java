package pizzashop.service;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import pizzashop.model.PaymentType;

import java.util.Optional;

public class PaymentAlert implements PaymentOperation {
    protected static final Logger log = LogManager.getLogger(PaymentAlert.class);
    private final PizzaService service;
    private static final String SEPARATOR = "--------------------------";

    public PaymentAlert(PizzaService service){
        this.service=service;
    }

    @Override
    public void cardPayment() {
        log.info(SEPARATOR);
        log.info("Paying by card...");
        log.info("Please insert your card!");
        log.info(SEPARATOR);
    }
    @Override
    public void cashPayment() {
        log.info(SEPARATOR);
        log.info("Paying cash...");
        log.info("Please show the cash...!");
        log.info(SEPARATOR);
    }
    @Override
    public void cancelPayment() {
        log.info(SEPARATOR);
        log.info("Payment choice needed...");
        log.info(SEPARATOR);
    }
      public void showPaymentAlert(int tableNumber, double totalAmount ) {
        Alert paymentAlert = new Alert(Alert.AlertType.CONFIRMATION);
        paymentAlert.setTitle("Payment for Table "+tableNumber);
        paymentAlert.setHeaderText("Total amount: " + totalAmount);
        paymentAlert.setContentText("Please choose payment option");
        ButtonType cardPayment = new ButtonType("Pay by Card");
        ButtonType cashPayment = new ButtonType("Pay Cash");
        ButtonType cancel = new ButtonType("Cancel");
        paymentAlert.getButtonTypes().setAll(cardPayment, cashPayment, cancel);
        Optional<ButtonType> result = paymentAlert.showAndWait();
        if (result.isEmpty()) {
            log.error("Close result optional was empty");
            return;
        }
        if (result.get() == cardPayment) {
            cardPayment();
            service.addPayment(tableNumber, PaymentType.CARD,totalAmount);
        } else if (result.get() == cashPayment) {
            cashPayment();
            service.addPayment(tableNumber, PaymentType.CASH,totalAmount);
        } else if (result.get() == cancel) {
             cancelPayment();
        } else {
            cancelPayment();
        }
    }
}