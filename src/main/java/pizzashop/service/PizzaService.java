package pizzashop.service;

import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.ArrayList;
import java.util.List;

public class PizzaService {

    private final MenuRepository menuRepo;
    private final PaymentRepository paymentRepository;

    public PizzaService(MenuRepository menuRepo, PaymentRepository paymentRepository) {
        this.menuRepo = menuRepo;
        this.paymentRepository = paymentRepository;
    }

    public List<MenuDataModel> getMenuData() {
        return menuRepo.getMenu();
    }

    public List<Payment> getPayments() {
        return paymentRepository.getAll();
    }

    public double getTotalPayments(PaymentType type) {
        if (paymentRepository.getAll().isEmpty()) {
            return -1;
        }
        var total = 0.0;
        int i = 0;
        while (i < paymentRepository.getAll().size()) {
            if (paymentRepository.getAll().get(i).getType() == type) {
                if (paymentRepository.getAll().get(i).getAmount() != 0) {
                    total += paymentRepository.getAll().get(i).getAmount();
                }
            }
            i++;
        }
        return total;
    }

    public void addPayment(int table, PaymentType type, double amount) {
        if (table < 1 || table > 8 || amount <= 0) {
            throw new RestaurantException("Invalid payment");
        }
        Payment payment = new Payment(table, type, amount);
        paymentRepository.add(payment);
    }

    public double getTotalAmount(PaymentType type) {
        double total = 0.0f;
        List<Payment> l = getPayments();
        if ((l == null) || (l.isEmpty())) return total;
        for (Payment p : l) {
            if (p.getType().equals(type))
                total += p.getAmount();
        }
        return total;
    }

}