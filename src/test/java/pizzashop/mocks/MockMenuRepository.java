package pizzashop.mocks;

import pizzashop.model.MenuDataModel;
import pizzashop.repository.MenuRepository;

import java.util.ArrayList;
import java.util.List;

public class MockMenuRepository extends MenuRepository {
    private final List<MenuDataModel> listMenu = new ArrayList<>();

    public MockMenuRepository() {
        listMenu.add(new MenuDataModel("Hawaii", 0, 7.49));
    }

    @Override
    public List<MenuDataModel> getMenu() {
        return listMenu;
    }
}
