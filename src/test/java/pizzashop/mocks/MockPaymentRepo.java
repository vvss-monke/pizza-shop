package pizzashop.mocks;

import pizzashop.model.Payment;
import pizzashop.repository.PaymentRepository;

import java.util.ArrayList;
import java.util.List;

public class MockPaymentRepo extends PaymentRepository {
    private final List<Payment> paymentList = new ArrayList<>();

    @Override
    public void add(Payment payment) {
        paymentList.add(payment);
    }

    @Override
    public List<Payment> getAll() {
        return paymentList;
    }

    @Override
    public void writeAll() {
    }
}
