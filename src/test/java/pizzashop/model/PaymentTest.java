package pizzashop.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PaymentTest {

    @Test
    void getTableNumber() {
        var payment = new Payment(1,PaymentType.CARD,0.1);
        assertEquals(1,payment.getTableNumber());
    }

    @Test
    void setTableNumber() {
        var payment = new Payment(1,PaymentType.CARD,0.1);
        payment.setTableNumber(2);
        assertEquals(2,payment.getTableNumber());
    }

    @Test
    void testToString() {
        var payment = new Payment(1,PaymentType.CARD,0.1);
        assertEquals("1,CARD,0.1",payment.toString());
    }
}