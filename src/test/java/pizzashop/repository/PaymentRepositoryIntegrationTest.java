package pizzashop.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;
import pizzashop.service.PizzaService;
import pizzashop.service.RestaurantException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

public class PaymentRepositoryIntegrationTest {
    @Mock
    private MenuRepository menuRepository;

    @Mock
    private PaymentRepository paymentRepository;
    @InjectMocks
    private PizzaService service;

    private Payment payment;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addException() {
        payment = mock(Payment.class);
        try {
            service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());
        } catch (Exception e) {

        }
        Mockito.verify(paymentRepository, Mockito.times(0)).add(payment);
    }

    @Test
    void getMenuData() {
        service.getMenuData();
        Mockito.verify(menuRepository, Mockito.times(1)).getMenu();
    }

    @Test
    void getAll() {
        service.getPayments();
        Mockito.verify(paymentRepository, Mockito.times(1)).getAll();
    }

}
