package pizzashop.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PaymentRepositoryMockitoTest {

    @Mock
    private PaymentRepository paymentRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void add() {
        var payment = new Payment(1, PaymentType.CARD, 0.1);
        Mockito.doNothing().when(paymentRepository).add(payment);
        paymentRepository.add(payment);
        Mockito.verify(paymentRepository, Mockito.times(1)).add(payment);
    }

    @Test
    void getAll() {
        Mockito.doReturn(List.of(new Payment(1, PaymentType.CARD, 0.1), new Payment(1, PaymentType.CASH, 2.1))).when(paymentRepository).getAll();
        assertEquals(2, paymentRepository.getAll().size());
    }
}