package pizzashop.repository;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PaymentRepositoryTest {

    @Test
    void getAll() {
        var repo = new PaymentRepository();
        assertEquals(12, repo.getAll().size());
    }

    @Test
    void getOne() {
        var repo = new PaymentRepository();
        assertEquals(4, repo.get(0).getTableNumber());
    }
}
