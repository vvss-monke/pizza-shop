package pizzashop.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PizzaServiceIntegrationTest {

    private PizzaService pizzaService;

    @BeforeEach
    void setUp(){
        pizzaService = new PizzaService(new MenuRepository(),new PaymentRepository());
    }

    @Test
    void getAll() {
        assertEquals(12, pizzaService.getPayments().size());
    }

    @Test
    void getOne() {
        assertEquals(317.55, pizzaService.getTotalAmount(PaymentType.CARD));
    }
}
