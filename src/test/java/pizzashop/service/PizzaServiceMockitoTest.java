package pizzashop.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import static org.junit.jupiter.api.Assertions.*;

class PizzaServiceMockitoTest {

    @Mock
    private PaymentRepository paymentRepository;
    @Mock
    private MenuRepository menuRepo;

    @InjectMocks
    private PizzaService pizzaService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addException(){
        Mockito.doThrow(RestaurantException.class).when(paymentRepository).add(new Payment(1, PaymentType.CARD,0));
        assertThrows(RestaurantException.class, () -> pizzaService.addPayment(1, PaymentType.CARD, 0));
    }

    @Test
    void add(){
        var payment = new Payment(1,PaymentType.CARD,0.1);
        Mockito.doNothing().when(paymentRepository).add(payment);
        pizzaService.addPayment(1,PaymentType.CARD,0.1);
        Mockito.verify(paymentRepository, Mockito.times(1)).add(payment);
    }
}