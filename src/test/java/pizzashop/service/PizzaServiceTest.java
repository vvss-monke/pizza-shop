package pizzashop.service;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pizzashop.mocks.MockMenuRepository;
import pizzashop.mocks.MockPaymentRepo;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepository;

import java.util.List;

import static java.lang.Double.MAX_VALUE;
import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PizzaServiceTest {

    private PizzaService pizzaService;

    void addToRepo(List<Payment> paymentList) throws Exception {
        var repoField = PizzaService.class.getDeclaredField("paymentRepository");
        repoField.setAccessible(true);
        PaymentRepository repo = (PaymentRepository) repoField.get(pizzaService);
        for (var payment : paymentList) {
            repo.add(payment);
        }
    }

    @BeforeEach
    void setUp() {
        pizzaService = new PizzaService(new MockMenuRepository(), new MockPaymentRepo());
    }

    @Disabled
    @Test
    void disabledTest() {
        pizzaService.addPayment(0, PaymentType.CARD, 0);
    }

    @DisplayName("Test all valid values for table ns")
    @Order(1)
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8})
    @Tag("ECP")
    void addPaymentEcpValidTableNr(Integer tableNr) {
        pizzaService.addPayment(tableNr, PaymentType.CARD, 1);
        assertEquals(1, pizzaService.getPayments().size());
    }

    @Test
    @Tag("ECP")
    void tableNrInvalid() {
        assertThrows(RestaurantException.class, () -> pizzaService.addPayment(0, PaymentType.CARD, 1));
    }

    @Test
    @Tag("ECP")
    void QuantityInvalid() {
        assertThrows(RestaurantException.class, () -> pizzaService.addPayment(1, PaymentType.CARD, 0));
    }

    @Test
    @Tag("ECP")
    void tableNrAndQuantityValid() {
        pizzaService.addPayment(1, PaymentType.CARD, 1);
        assertEquals(1, pizzaService.getPayments().size());
    }

    @ParameterizedTest
    @ValueSource(doubles = {0, 0.1, MAX_VALUE})
    @Tag("BVA")
    void InvalidTableNrLower(double quantity) {
        assertThrows(RestaurantException.class, () -> pizzaService.addPayment(0, PaymentType.CARD, quantity));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 8})
    @Tag("BVA")
    void validTableNrInvalidQuantity(Integer tableNr) {
        assertThrows(RestaurantException.class, () -> pizzaService.addPayment(tableNr, PaymentType.CARD, 0));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 8})
    @Tag("BVA")
    void ValidTableNrValidQuantity(Integer tableNr) {
        pizzaService.addPayment(tableNr, PaymentType.CARD, 0.1);
        assertEquals(1, pizzaService.getPayments().size());
    }

    @ParameterizedTest
    @ValueSource(doubles = {0, 0.1})
    @Tag("BVA")
    void InvalidTableNrGreater(double quantity) {
        assertThrows(RestaurantException.class, () -> pizzaService.addPayment(9, PaymentType.CARD, quantity));
    }

    @Test
    void getPaymentsCashEmptyList() {
        assertEquals(-1, pizzaService.getTotalPayments(PaymentType.CASH));
    }

    @Test
    void getPaymentsCardEmptyList() {
        assertEquals(-1, pizzaService.getTotalPayments(PaymentType.CARD));
    }

    @Test
    void getPaymentsCashNoPaymentOfType() {
        pizzaService.addPayment(1, PaymentType.CARD, 5);
        assertEquals(0, pizzaService.getTotalPayments(PaymentType.CASH));
    }

    @Test
    void getPaymentsCardNoPaymentOfType() {
        pizzaService.addPayment(1, PaymentType.CASH, 5);
        assertEquals(0, pizzaService.getTotalPayments(PaymentType.CARD));
    }

    @Test
    void getPaymentsCashZeroValue() throws Exception {
        addToRepo(List.of(new Payment(1, PaymentType.CASH, 0)));
        assertEquals(0, pizzaService.getTotalPayments(PaymentType.CASH));
    }

    @Test
    void getPaymentsCardZeroValue() throws Exception {
        addToRepo(List.of(new Payment(1, PaymentType.CARD, 0)));
        assertEquals(0, pizzaService.getTotalPayments(PaymentType.CARD));
    }

    @Test
    void getTotalPaymentsCash() {
        assertDoesNotThrow(() -> {
            addToRepo(List.of(new Payment(1, PaymentType.CARD, 5),
                    new Payment(1, PaymentType.CASH, 6),
                    new Payment(1, PaymentType.CASH, 9)));
            assertEquals(15, pizzaService.getTotalPayments(PaymentType.CASH));
        });
    }

    @Test
    void getTotalPaymentsCard() throws Exception {
        addToRepo(List.of(new Payment(1, PaymentType.CASH, 5),
                new Payment(1, PaymentType.CARD, 6),
                new Payment(1, PaymentType.CARD, 9)));
        assertEquals(15, pizzaService.getTotalPayments(PaymentType.CARD));
    }
}
